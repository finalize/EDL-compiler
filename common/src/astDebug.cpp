/*
@file: astDebug.cpp
@author: ZZH
@time: 2021-12-27 15:26:20
@info: 抽象语法树调试信息
*/
#include "ast.h"
#define FULL_DEBUG 0

const char* ASTNode_t::nodeType2Str[] = {
    "None",
    "Expression",
    "VariableDef",
    "FunctionDef",
};

const char* ASTExpression_t::expType2Str[] = {
    "Eval",
    "Number",
    "String",
    "VariableCall",
    "FunctionCall",
    "Block"
};

namespace dataTypes {
    const char* dataType2Str[] = {
    "None",
    "Auto",
    "Bool",
    "Byte",
    "Int",
    "Float",
    "String",
    };
}


void ASTExpression_t::printInfo() const
{
#if FULL_DEBUG == 1
    this->ASTNode_t::printInfo();//先调用父类信息输出
#endif

    RAW("[ASTExpression]\r\n    expType:%s", this->getExpTypeStr());
}

void ASTBlockStatement_t::printInfo() const
{
#if FULL_DEBUG == 1
    this->ASTExpression_t::printInfo();//先调用父类信息输出
#endif

    RAW("[ASTBlockStatement]");
    for (auto var : this->localVarTable)
        var.second->printInfo();
    
    for (auto exp : this->expList)
        exp->printInfo();
    RAW("[ASTBlockStatement End]");
}

void ASTVariableDef_t::printInfo() const
{
#if FULL_DEBUG == 1
    this->ASTNode_t::printInfo();//先调用父类信息输出
#endif

    RAW("[ASTVariableDef]\r\n    %s:%s @ %#X", this->varName.c_str(), this->getVarTypeStr(), this->addr);
}

void ASTFunctionDef_t::printInfo() const
{
#if FULL_DEBUG == 1
    this->ASTNode_t::printInfo();//先调用父类信息输出
#endif

    RAW("[ASTFunctionDef]\r\n    fun_name:%s", this->funcName.c_str());
    this->funcBody->printInfo();
}

void ASTVariableCall_t::printInfo() const
{
#if FULL_DEBUG == 1
    this->ASTExpression_t::printInfo();//先调用父类信息输出
#endif

    RAW("[ASTVariableCall_t]\r\n    varName:%s", this->varName.c_str());
}

void ASTFunctionCall_t::printInfo() const
{
#if FULL_DEBUG == 1
    this->ASTExpression_t::printInfo();//先调用父类信息输出
#endif

    RAW("[ASTFunctionCall_t]\r\n    funcName:%s", this->funcName.c_str());
    if (this->pArgs)
    {
        RAW("----print args");
        for (auto exp : *this->pArgs)
            exp->printInfo();
    }
    else
        RAW("----no args");
}

void ASTOperatorDouble_t::printInfo() const
{
#if FULL_DEBUG == 1
    this->ASTExpression_t::printInfo();//先调用父类信息输出
#endif

    RAW("[ASTOperatorDouble]\r\n    Operator:%c", this->op);
    if (this->left_exp)
    {
        RAW("----enter left");
        this->left_exp->printInfo();
    }
    else
        ERROR("left is NULL !!!");

    if (this->right_exp)
    {
        RAW("----enter right");
        this->right_exp->printInfo();
    }
    else
        ERROR("right is NULL !!!");
}

void ASTNumber_t::printInfo() const
{
#if FULL_DEBUG == 1
    this->ASTExpression_t::printInfo();//先调用父类信息输出
#endif

    RAW("[ASTNumber]\r\n    value:%d", this->value);
}

void ASTString_t::printInfo() const
{
#if FULL_DEBUG == 1
    this->ASTExpression_t::printInfo();//先调用父类信息输出
#endif

    RAW("[ASTString]\r\n    str:%s", this->str);
}
