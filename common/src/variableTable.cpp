#include "variableTable.h"
VariableTable_t VariableTable;

bool VariableTable_t::search(const string& str, ASTVariableDef_t*& pVarDef) const
{
    ASTVariableDef_t* res = nullptr;

    //首先在当前作用域内查找使用到的变量
    if (this->curVariableTable->search(str, res))
    {
        pVarDef = res;
        return true;
    }

    //如果当前作用域没有,逐级退栈回到上一层作用域查找变量
    for (auto rit = this->stack.rbegin(); rit != this->stack.rend(); rit++)
    {
        if ((*rit)->search(str, res))
        {
            pVarDef = res;
            return true;
        }
    }

    //如果最外层的局部作用域都没有,就在全局作用域查找
    if (this->glbVariableTable.search(str, res))
    {
        pVarDef = res;
        return true;
    }

    //所有的作用域内都没有找到此变量,说明变量未定义
    return false;
}
