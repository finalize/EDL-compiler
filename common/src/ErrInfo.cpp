/*
@file: ErrInfo.cpp
@author: ZZH
@time: 2021-12-28 22:12:50
@info:
*/
#include "ErrInfo.h"
#include <cstdio>
#include <cstdarg>

GrammarError::GrammarError(const char* fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    char buf[256];
    uint32_t numOfChar = vsnprintf(buf, sizeof(buf), fmt, ap);
    buf[numOfChar] = '\0';
    this->msg += buf;
    va_end(ap);
}
