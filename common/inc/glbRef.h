/*
@file: glbRef.h
@author: ZZH
@time: 2021-12-29 16:04:41
@info: 声明全局的数据结构
*/
#pragma once
#include "ast.h"
#include "symTable.h"
#include "variableTable.h"
#include "functionTable.h"
#include <stack>
using namespace std;

extern list<ASTNode_t*> astList;

int yyerror(char* s);
void yyrestart(FILE* input_file);
extern int yylex (void);
