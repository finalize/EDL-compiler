/*
@file: functionTable.h
@author: ZZH
@time: 2021-12-31 11:13:40
@info: 基于符号表的全局函数表
*/
#pragma once
#include "ast.h"
#include "symTable.h"
#include <list>
#include <string>
#include <iostream>
using namespace std;

class FunctionTable_t
{
public:
    using FunctionSymTable_t = SymTable_t<ASTFunctionDef_t*>;
private:

protected:
    FunctionSymTable_t table;
public:
    using iterator = FunctionSymTable_t::iterator;
    using const_iterator = FunctionSymTable_t::const_iterator;
    using size_type = FunctionSymTable_t::size_type;

    FunctionTable_t() {}
    ~FunctionTable_t()
    {
        cout << "clear glbFunctionTable" << endl;
        //!!!这里不要清table,因为table是符号表,本身会管理内部元素
        // for (auto func : this->table)
        //     delete func.second;
    }

    inline bool insert(const string& str, ASTFunctionDef_t* pFunDef) { return this->table.insert(str, pFunDef); }
    inline bool remove(const string& str) { return this->table.remove(str); }
    inline bool search(const string& str, ASTFunctionDef_t*& pFunDef) { return this->table.search(str, pFunDef); }
    inline iterator begin() { return this->table.begin(); }
    inline iterator end() { return this->table.end(); }
    inline size_type size() { return this->table.size(); }
    inline size_type len() { return this->table.len(); }
};
extern FunctionTable_t FunctionTable;
