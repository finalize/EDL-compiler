/*
@file: ErrInfo.h
@author: ZZH
@time: 2021-12-28 22:07:58
@info: ������Ϣ����
*/
#pragma once
#include <exception>
#include <string>
using namespace std;

class GrammarError:public exception
{
private:

protected:
    string msg;
public:
    GrammarError(const char* fmt, ...) __attribute__((format(printf, 2, 3)));
    ~GrammarError() {}

    virtual const char* what() const noexcept { return msg.c_str(); }
};

extern const char* inst2str[];
